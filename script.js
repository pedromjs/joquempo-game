let playerPoints = 0;
let computerPoints = 0;
let winner = -1;

// const options = ['rock', 'paper', 'scissors', 'lizard', 'spock'];
// const whatHappens = [
//     'cuts', 'covers', 'smashes', 'poisons', 'melts',
//     'decapitates', 'eats', 'refutes', 'obliterates', 'crumples'
// ];


const play = (x) => {
    playerChoise = x;


    computerChoise = Math.floor(Math.random() * (5 - 1 + 1) + 1);

    if (
        (playerChoise == 1 && (computerChoise == 3)) ||
        (playerChoise == 1 && (computerChoise == 4)) ||
        (playerChoise == 2 && (computerChoise == 1)) ||
        (playerChoise == 2 && (computerChoise == 5)) ||
        (playerChoise == 3 && (computerChoise == 4)) ||
        (playerChoise == 3 && (computerChoise == 2)) ||
        (playerChoise == 4 && (computerChoise == 2)) ||
        (playerChoise == 4 && (computerChoise == 5)) ||
        (playerChoise == 5 && (computerChoise == 1)) ||
        (playerChoise == 5 && (computerChoise == 3))
    ) {
        winner = 1;
        playerPoints++;

    } else if (playerChoise === computerChoise) {
        winner = 0;

    } else {
        winner = 2;
        computerPoints++;
    }


    document.getElementById('player__choise-1').classList.remove('selected');
    document.getElementById('player__choise-2').classList.remove('selected');
    document.getElementById('player__choise-3').classList.remove('selected');
    document.getElementById('player__choise-4').classList.remove('selected');
    document.getElementById('player__choise-5').classList.remove('selected');
    document.getElementById('computer__choise-1').classList.remove('selected');
    document.getElementById('computer__choise-2').classList.remove('selected');
    document.getElementById('computer__choise-3').classList.remove('selected');
    document.getElementById('computer__choise-4').classList.remove('selected');
    document.getElementById('computer__choise-5').classList.remove('selected');



    let plChosen = document.getElementById('player__choise-' + playerChoise);
    plChosen.className = 'selected';

    let compChosen = document.getElementById('computer__choise-' + computerChoise);
    compChosen.className = 'selected';

    let plPoints = document.getElementById('player__points')
    plPoints.innerText = playerPoints;

    let compPoints = document.getElementById('computer__points')
    compPoints.innerText = computerPoints;


    if (playerPoints === 10 || computerPoints === 10) {
        document.getElementById('versus').innerHTML = 'We have a winner!';
        playerPoints = 0;
        computerPoints = 0;
    }

}